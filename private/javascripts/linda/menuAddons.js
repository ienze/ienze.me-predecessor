$(function () {
    $("#menu").css('height', $("#menu").height());


    var centerMask = function () {
        $(".mask").css({
            top: $("body").height() / 2 - 300,
            left: $("body").width() / 2 - 300
        });
    }

    $("#menu").mousemove(function (e) {
        $(".mask").css({
            top: e.pageY - 300,
            left: e.pageX - 300
        });
    });

    $("#menu").mouseleave(function (e) {
        centerMask();
    });

    centerMask();

    //passive switching
    $("#menu .passiveSwitch").click(function () {
        if ($(".metroNavigation").hasClass("active")) {
            $(this).text("Advanced menu");
        } else {
            $(this).text("Simple menu");
        }
        $(".metroNavigation").toggleClass("active passive");
    });
});
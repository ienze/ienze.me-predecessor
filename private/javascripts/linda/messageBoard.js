$(function () {
	var socket = null;
	var loginToken = null;

	$("#messageBoard .chat").hide();

	$("#messageBoard .login form").submit(function () {

		if (!socket) {
			Notification.requestPermission();

			if (io) {
				socket = io();
			} else {
				return;
			}

			//send
			$("#messageBoard .chat form").submit(function () {
				var message = $('#messageBoardInput').val();

				socket.emit('message', message);

				$('#messageBoardInput').val('');

				return false;
			});

			//recive
			socket.on('userok', function () {
				$("#messageBoard .login").remove();
				$("#messageBoard .chat").show();
			});

			socket.on('usernok', function () {
				loginToken = "-_-";
				$("#messageBoard h2").text("Username incorrect");
			});

			socket.on('message', function (message) {
				var messageBoardUl = $('#messageBoard .chat ul');
				messageBoardUl.append($('<li>').text(message));
				notifyHer(message);
				messageBoardUl.scrollTop(messageBoardUl[0].scrollHeight);
			});

			socket.on('userauth', function (token) {
				loginToken = token;
				$("#messageBoard .login input#messageBoardName").attr('type', 'password');
				$("#messageBoard .login h2").text("Type password");
			});
		}

		if (loginToken) {
			var pass = $("#messageBoardName").val();
			$("#messageBoardName").val('');
			socket.emit('userauth', pass + loginToken);
		} else {
			var username = $("#messageBoardName").val();
			$("#messageBoardName").val('');
			socket.emit('userlogin', username);
		}

		return false;
	});


	//notifications
	var windowActive = false;

	var notifyHer = function (message) {
		if (!windowActive) {
			var options = {
				body: message,
				tag: 'ienze.me'
			};
			var n = new Notification('ienze.me', options);
		}
	};

	$(window).on("blur", function (e) {
		windowActive = false;
	});
	$(window).on("focus", function (e) {
		windowActive = true;
	});
});
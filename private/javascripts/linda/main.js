$(function () {

	var alsoInDynamiclyLoaded = function () {
		try {
			jQuery("abbr.timeago").timeago();
		} catch (e) {
			console.log(e);
		}
	};
	alsoInDynamiclyLoaded();

	try {
		$('.jscroll').jscroll({
			autoTriggerUntil: 3,
			nextSelector: "a.pagination",
			loadingHtml: '<div class="loading"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>',
			callback: alsoInDynamiclyLoaded
		});
	} catch (e) {
		console.log(e);
	}

	try {
		$("#menu-open").sticky({topSpacing: 0});
	} catch (e) {
		console.log(e);
	}

	try {
		var metroNavigation = $(".metroNavigation");
		metroNavigation.metroNavigation();
		metroNavigation.metroNavigationGrab();
		//metroNavigation.metroNavigationPages();
	} catch (e) {
		console.log(e);
	}

	try {
		var panorama = $("#panorama .panorama-content");
		if (panorama.size()) {
			panorama.empty();

			var panoramaPortfolio = $('<ul class="portfolio"></ul>');
			panorama.append(panoramaPortfolio);

			panoramaPortfolio.load("/portfolio");
		}
	} catch (e) {
		console.log(e);
	}

	try {
		$(".language-change a").click(function () {

			var lang = $(this).attr('class');

			$.cookie('lang', lang, {path: '/', expires: 356});

			document.location.reload(true);

			return false;
		});
	} catch (e) {
		console.log(e);
	}

	try {
		$(".image-popup").each(function () {
			$(this).imagePopup();
		});

	} catch (e) {
		console.log(e);
	}

	try {
		$('a').each(function () {
			var a = new RegExp('/' + window.location.host + '/');
			var b = new RegExp('^http');
			if (!a.test(this.href) && b.test(this.href)) {
				$(this).attr("target", "_blank");
				$(this).addClass("externalLink");
			}
		});
	} catch (e) {
		console.log(e);
	}
});
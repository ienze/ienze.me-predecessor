var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');

router.get('/', function (req, res, next) {
	res.status(404);
	res.render('php', siteSettings.settings);
});

module.exports = router;
var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');

/* GET home page. */
router.get('/', function (req, res, next) {

    var renderSettings = siteSettings.extend({}, siteSettings.settings, {
		title: 'About',
        activeMenuItem: 'about',
        lang: siteSettings.lang(req),
		siteDescription: 'SITE_DESCRIPTION_ABOUT',
		siteKeywords: 'SITE_KEYWORDS_ABOUT'
    });

    res.render('about', renderSettings);
});

module.exports = router;

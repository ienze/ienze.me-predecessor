var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');

router.get('/', function (req, res, next) {

    var renderSettings = siteSettings.extend({}, siteSettings.settings, {
        title: 'MessageBoard',
        activeMenuItem: 'messageBoard',
        lang: siteSettings.lang(req)
    });

    res.render('messageBoard', renderSettings);
});

if (siteSettings.settings.siteMessageBoardEnabled) {
    module.exports = router;
} else {
    module.exports = null;
}
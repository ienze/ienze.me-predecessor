var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    
    var renderSettings = siteSettings.extend({}, siteSettings.settings, {
        lang: siteSettings.lang(req)
    });

    res.render('index', renderSettings);
});

module.exports = router;

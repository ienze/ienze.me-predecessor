var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');
var content = require('../bin/content.js');
var namp = require('namp');

var ITEMS_PER_PAGE = 8;

var isNormalInteger = function (str) {
	return /^\+?(0|[1-9]\d*)$/.test(str);
};

var markdownContent = function (post) {
	post.text = namp(post.text).html;
	return post;
};

var controllerSingle = function (req, res, next, id) {

	var isAjax = req.xhr;
	var template = 'project';
	if (isAjax) {
		template = template + 'Ajax';
	}

	content.dataLoaders.project.fetchOne(id, function (data) {

		var renderSettings = siteSettings.extend({}, siteSettings.settings, {
			title: 'Project',
			activeMenuItem: "portfolio",
			project: markdownContent(data),
			lang: siteSettings.lang(req)
		});
		
		if (data.description) {
			renderSettings.siteDescription = data.description;
		}
		if (data.keywords) {
			renderSettings.siteKeywords = data.keywords;
		}
		
		res.render(template, renderSettings);

	}, function (data) {
		content.printError(data, next);
	}, {
		'locale': siteSettings.lang(req).dataLang
	});
};


router.get('/:project', function (req, res, next) {
	controllerSingle(req, res, next, req.params.project);
});

module.exports = router;

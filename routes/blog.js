var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');
var content = require('../bin/content.js');
var namp = require('namp');

var ITEMS_PER_PAGE = 3;

var isNormalInteger = function (str) {
	return /^\+?(0|[1-9]\d*)$/.test(str);
};

var markdownContent = function (post) {
	post.text = namp(post.text).html;
	return post;
};

var markdownContents = function (posts) {
	for (var index in posts) {
		posts[index] = markdownContent(posts[index]);
	}
	return posts;
};

var controller = function (req, res, next, arg) {

	var isAjax = req.xhr;

	var page = 1;
	var id = null;

	if (isNormalInteger(arg)) {
		page = parseInt(arg);
	} else {
		id = arg;
	}

	if (page < 1) {
		page = 1;
	}

	var renderSettings = siteSettings.extend({}, siteSettings.settings, {
		activeMenuItem: 'blog',
		title: 'Blog',
		lang: siteSettings.lang(req)
	});

	if (id) {

		content.dataLoaders.blog.fetchOne(id, function (data) {
			
			var singleRenderSettings = siteSettings.extend({}, renderSettings, {
				post: markdownContent(data)
			});
			
			if(data.description) {
				singleRenderSettings.siteDescription = data.description;
			}
			if(data.keywords) {
				singleRenderSettings.siteKeywords = data.keywords;
			}
			
			res.render('post', singleRenderSettings);
		}, function (data) {
			content.printError(data, next);
		}, {
			'locale': siteSettings.lang(req).dataLang
		});

	} else {

		var template = 'blog';
		if (isAjax) {
			template = template + 'Ajax';
		}

		content.dataLoaders.blog.fetchMore((page - 1) * ITEMS_PER_PAGE, ITEMS_PER_PAGE, function (data) {
			res.render(template, siteSettings.extend({}, renderSettings, {
				posts: markdownContents(data),
				page: page,
				siteDescription: 'SITE_DESCRIPTION_BLOG',
				siteKeywords: 'SITE_KEYWORDS_BLOG'
			}));
		}, function (data) {
			content.printError(data, next);
		}, {
			'order': '-fields.date',
			'locale': siteSettings.lang(req).dataLang
		});
	}
};

var controllerFeed = function (req, res, next) {

	content.dataLoaders.blog.fetchMore(0, 20, function (data) {
		res.render('blogFeed', siteSettings.extend({}, siteSettings.settings, {
			posts: data
		}));
	}, function (data) {
		content.printError(data, next);
	}, {
		'order': '-fields.date',
		'locale': siteSettings.lang(req).dataLang
	});
};

router.get('/feed', function (req, res, next) {
	controllerFeed(req, res, next);
});
router.get('/:arg', function (req, res, next) {
	controller(req, res, next, req.params.arg);
});
router.get('/', function (req, res, next) {
	controller(req, res, next, null);
});

module.exports = router;

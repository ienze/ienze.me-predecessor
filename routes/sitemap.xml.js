var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');
var content = require('../bin/content.js');

var ITEMS_PER_QUERY = 100;

var loadProjects = function (req, urls, urlsCallback, f) {

	if (!f) {
		f = 0;
	}

	content.dataLoaders.project.fetchMore(f * ITEMS_PER_QUERY, ITEMS_PER_QUERY, function (data) {

		//TODO resCount;
		var resCount = data.length;

		if (resCount == 0) {
			urlsCallback(urls);
		} else {

			for (var index in data) {
				urls.push('project/' + data[index].slug);
			}

			if (resCount == ITEMS_PER_QUERY) {
				loadProjects(req, urls, urlsCallback, f + 1);
			} else {
				urlsCallback(urls);
			}
		}

	}, function (data) {
		content.printError(data, null);

	}, {
		'order': '-fields.startedAt',
		'locale': siteSettings.lang(req).dataLang
	});
};

var loadBlogs = function (req, urls, urlsCallback, f) {

	if (!f) {
		f = 0;
	}

	content.dataLoaders.blog.fetchMore(f * ITEMS_PER_QUERY, ITEMS_PER_QUERY, function (data) {

		//TODO resCount;
		var resCount = data.length;

		if (resCount == 0) {
			urlsCallback(urls);
		} else {

			for (var index in data) {
				urls.push('blog/' + data[index].slug);
			}

			if (resCount == ITEMS_PER_QUERY) {
				loadBlogs(req, urls, urlsCallback, f + 1);
			} else {
				urlsCallback(urls);
			}
		}

	}, function (data) {
		content.printError(data, null);

	}, {
		'locale': siteSettings.lang(req).dataLang
	});
};

//setup content type
router.get(function (req, res, next) {
	res.header("Content-Type", "application/xml");
	next();
});

router.get('/', function (req, res, next) {

	var urls = [];
	var staticUrls = [];

	staticUrls.push('');
	staticUrls.push('about/');
	staticUrls.push('blog/');
	staticUrls.push('portfolio/');
	staticUrls.push('portfolio/programming/');
	staticUrls.push('portfolio/websites/');
	staticUrls.push('portfolio/art/');
	staticUrls.push('portfolio/other/');

	loadProjects(req, urls, function (urls) {
		loadBlogs(req, urls, function (urls) {
			
			res.type('application/xml');
			res.render('sitemap', {
				siteUrl: 'http://ienze.me/',
				staticUrls: staticUrls,
				urls: urls
			});
			
		}, 0);
	}, 0);

});

module.exports = router;
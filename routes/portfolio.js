var express = require('express');
var router = express.Router();
var siteSettings = require('../bin/siteSettings.js');
var content = require('../bin/content.js');
var namp = require('namp');

var ITEMS_PER_PAGE = 8;

var isNormalInteger = function (str) {
	return /^\+?(0|[1-9]\d*)$/.test(str);
};

var markdownContent = function (post) {
	post.text = namp(post.text).html;
	return post;
};

var sortProjects = function (projectsIn, random) {
	var projectsOut = [];

	var p = 0;
	var b = 0;
	while (p < projectsIn.length) {
		var r = 1 + Math.floor(random() * 4);
		if (r === 3)
			r = 4;

		projectsOut[b] = projectsIn.slice(p, p + r);

		p += r;
		b++;
	}
    
	return projectsOut;
};

var controllerMany = function (req, res, next, portfolioCategory, pageArg) {

	var isAjax = req.xhr;
	var page = 1;
	if (pageArg && isNormalInteger(pageArg)) {
		page = parseInt(pageArg);
	}
	if (page < 1) {
		page = 1;
	}

	var template = 'portfolio';
	if (isAjax) {
		template = template + 'Ajax';
	}

	content.dataLoaders.project.fetchMore((page - 1) * ITEMS_PER_PAGE, ITEMS_PER_PAGE, function (data) {

		var seedrandom = require('seedrandom');
        
		var random = seedrandom(""+Math.floor(new Date().getTime() / 3600000)+page);

		var sortedProjects = sortProjects(data, random);

		var renderSettings = siteSettings.extend({}, siteSettings.settings, {
			title: 'Portfolio',
			activeMenuItem: portfolioCategory ? portfolioCategory : "portfolio",
			projects: sortedProjects,
			random: random,
			portfolioCategory: portfolioCategory,
			page: page,
			lang: siteSettings.lang(req),
			siteDescription: "SITE_DESCRIPTION_PORTFOLIO",
			siteKeywords: "SITE_KEYWORDS_PORTFOLIO" + (portfolioCategory ? "_" + portfolioCategory.toUpperCase() : ""),
			portfolioContent: "PORTFOLIO_CONTENT" + (portfolioCategory ? "_" + portfolioCategory.toUpperCase() : ""),
		});

		res.render(template, renderSettings);

	}, function (data) {
		content.printError(data, next);

	}, {
		'fields.category': portfolioCategory,
		'order': '-fields.startedAt',
		'locale': siteSettings.lang(req).dataLang
	});
};

var controllerSingle = function (req, res, next, id) {
	return res.redirect(301, '/project/' + id);
};

var controllerFeed = function (req, res, next) {

	content.dataLoaders.project.fetchMore(0, 20, function (data) {

		var renderSettings = siteSettings.extend({}, siteSettings.settings, {
			projects: data,
		});

		res.render('portfolioFeed', renderSettings);

	}, function (data) {
		content.printError(data, next);

	}, {
		'order': '-fields.startedAt',
		'locale': siteSettings.lang(req).dataLang
	});
};

router.get('/feed', function (req, res, next) {
	controllerFeed(req, res, next);
});

router.get('/project/:project', function (req, res, next) {
	controllerSingle(req, res, next, req.params.project);
});

router.get('/:category/:page', function (req, res, next) {
	controllerMany(req, res, next, req.params.category, req.params.page);
});

router.get('/:arg', function (req, res, next) {

	var arg = req.params.arg;

	if (isNormalInteger(arg)) {
		controllerMany(req, res, next, null, arg); //arg = page
	} else {
		controllerMany(req, res, next, arg); //arg = category
	}
});

router.get('/', function (req, res, next) {
	controllerMany(req, res, next, null, null);
});

module.exports = router;

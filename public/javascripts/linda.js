$(function () {
    var pressedFour = false;
    var title = $("#title-screen h1");

    var htmlreplace = function (a,b,element){if(!element)element=document.body;var nodes=element.childNodes;for(var n=0;n<nodes.length;n++){if(nodes[n].nodeType==Node.TEXT_NODE){nodes[n].textContent=nodes[n].textContent.replace(new RegExp(a,'gi'),b);}else{htmlreplace(a,b,nodes[n]);}}};

    var fourtyTwo = function () {
        htmlreplace('Ienze','Linda');
        title.addClass('butterfly-effect');
    };

    $('body').keydown(function (e) {

        if (e.which === 100 || e.which === 52) { // 4
            pressedFour = true;
        } else {
            if (pressedFour && (e.which === 98 || e.which === 50)) { // 2
                fourtyTwo();
            }
            pressedFour = false;
        }
    });
});;$.fn.imagePopup = function () {
    var image = this;

    var open = function (image, e) {
        
        var url = image.attr("href");

        var insideImage = $("<div></div>");
        insideImage.css({
            'background-image': 'url(' + url + ')',
            'background-size': 'cover',
            'z-index': '9999',
            position: 'fixed',
            top: image.offset().top - $(window).scrollTop(),
            left: image.offset().left - $(window).scrollLeft(),
            width: image.width(),
            height: image.height()
        });

        image.append(insideImage);

        insideImage.animate({
            top: 0,
            left: 0,
            width: '100%',
            height: '100%'
        }, 300);

        var center = function (e) {
            var x = (e.pageX - $(window).scrollLeft()) / $(window).width() * 100;
            var y = (e.pageY - $(window).scrollTop()) / $(window).height() * 100;

            insideImage.css('background-position', x + '% ' + y + '%');
        }

        insideImage.mousemove(function (e) {
            center(e);
        });

        center(e);
    };

    var close = function () {
        image.find("div").animate({
            top: image.offset().top - $(window).scrollTop(),
            left: image.offset().left - $(window).scrollLeft(),
            width: image.width(),
            height: image.height()
        }, 300, null, function () {
            image.find("div").remove();
        });
    };

    image.click(function (e) {

        if (image.find("div").size() === 0) {
            open(image, e);
        } else {
            close(image);
        }

        return false;
    });
};;$(function () {

	var alsoInDynamiclyLoaded = function () {
		try {
			jQuery("abbr.timeago").timeago();
		} catch (e) {
			console.log(e);
		}
	};
	alsoInDynamiclyLoaded();

	try {
		$('.jscroll').jscroll({
			autoTriggerUntil: 3,
			nextSelector: "a.pagination",
			loadingHtml: '<div class="loading"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>',
			callback: alsoInDynamiclyLoaded
		});
	} catch (e) {
		console.log(e);
	}

	try {
		$("#menu-open").sticky({topSpacing: 0});
	} catch (e) {
		console.log(e);
	}

	try {
		var metroNavigation = $(".metroNavigation");
		metroNavigation.metroNavigation();
		metroNavigation.metroNavigationGrab();
		//metroNavigation.metroNavigationPages();
	} catch (e) {
		console.log(e);
	}

	try {
		var panorama = $("#panorama .panorama-content");
		if (panorama.size()) {
			panorama.empty();

			var panoramaPortfolio = $('<ul class="portfolio"></ul>');
			panorama.append(panoramaPortfolio);

			panoramaPortfolio.load("/portfolio");
		}
	} catch (e) {
		console.log(e);
	}

	try {
		$(".language-change a").click(function () {

			var lang = $(this).attr('class');

			$.cookie('lang', lang, {path: '/', expires: 356});

			document.location.reload(true);

			return false;
		});
	} catch (e) {
		console.log(e);
	}

	try {
		$(".image-popup").each(function () {
			$(this).imagePopup();
		});

	} catch (e) {
		console.log(e);
	}

	try {
		$('a').each(function () {
			var a = new RegExp('/' + window.location.host + '/');
			var b = new RegExp('^http');
			if (!a.test(this.href) && b.test(this.href)) {
				$(this).attr("target", "_blank");
				$(this).addClass("externalLink");
			}
		});
	} catch (e) {
		console.log(e);
	}
});;$(function () {
    $("#menu").css('height', $("#menu").height());


    var centerMask = function () {
        $(".mask").css({
            top: $("body").height() / 2 - 300,
            left: $("body").width() / 2 - 300
        });
    }

    $("#menu").mousemove(function (e) {
        $(".mask").css({
            top: e.pageY - 300,
            left: e.pageX - 300
        });
    });

    $("#menu").mouseleave(function (e) {
        centerMask();
    });

    centerMask();

    //passive switching
    $("#menu .passiveSwitch").click(function () {
        if ($(".metroNavigation").hasClass("active")) {
            $(this).text("Advanced menu");
        } else {
            $(this).text("Simple menu");
        }
        $(".metroNavigation").toggleClass("active passive");
    });
});;$(function () {
	var socket = null;
	var loginToken = null;

	$("#messageBoard .chat").hide();

	$("#messageBoard .login form").submit(function () {

		if (!socket) {
			Notification.requestPermission();

			if (io) {
				socket = io();
			} else {
				return;
			}

			//send
			$("#messageBoard .chat form").submit(function () {
				var message = $('#messageBoardInput').val();

				socket.emit('message', message);

				$('#messageBoardInput').val('');

				return false;
			});

			//recive
			socket.on('userok', function () {
				$("#messageBoard .login").remove();
				$("#messageBoard .chat").show();
			});

			socket.on('usernok', function () {
				loginToken = "-_-";
				$("#messageBoard h2").text("Username incorrect");
			});

			socket.on('message', function (message) {
				var messageBoardUl = $('#messageBoard .chat ul');
				messageBoardUl.append($('<li>').text(message));
				notifyHer(message);
				messageBoardUl.scrollTop(messageBoardUl[0].scrollHeight);
			});

			socket.on('userauth', function (token) {
				loginToken = token;
				$("#messageBoard .login input#messageBoardName").attr('type', 'password');
				$("#messageBoard .login h2").text("Type password");
			});
		}

		if (loginToken) {
			var pass = $("#messageBoardName").val();
			$("#messageBoardName").val('');
			socket.emit('userauth', pass + loginToken);
		} else {
			var username = $("#messageBoardName").val();
			$("#messageBoardName").val('');
			socket.emit('userlogin', username);
		}

		return false;
	});


	//notifications
	var windowActive = false;

	var notifyHer = function (message) {
		if (!windowActive) {
			var options = {
				body: message,
				tag: 'ienze.me'
			};
			var n = new Notification('ienze.me', options);
		}
	};

	$(window).on("blur", function (e) {
		windowActive = false;
	});
	$(window).on("focus", function (e) {
		windowActive = true;
	});
});;$.fn.metroNavigation = function (settingsIn) {

    var metroNavigation = this;

    var settings = $.extend({}, {
        enabled: true,
        only_connected_nodes: true,
        move_speed: 800,
        auto_skip_to_content: true,
        skip_to_content_speed: 1000
    }, settingsIn);

    if (settings.auto_skip_to_content) {
        if (window.location.hash === '#m') {
            $("html, body").animate({scrollTop: $("body").height()}, settings.skip_to_content_speed);
        } else {
            $("html, body").animate({scrollTop: $("body").height()}, 0);
        }
    }

    var findNodeByMouse = function (metroNavigation, activeNode, x, y) {

        var nodes = findConnectedNodes(metroNavigation, activeNode);

        var closestNode = null;
        var closestNodeDistance = -1;

        $.each(nodes, function (index, node) {

            var jnode = $(node);

            var no = jnode.offset();

            var dx = Math.abs(x - no.left);
            var dy = Math.abs(y - no.top);
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (closestNodeDistance < 0 || distance < closestNodeDistance) {
                closestNodeDistance = distance;
                closestNode = jnode;
            }
        });

        return closestNode;
    };

    var findActiveNode = function (metroNavigation) {
        var active = metroNavigation.find('li.active');
        if (active.size()) {
            return active;
        } else {
            var newActive = metroNavigation.find('li').first();
            newActive.addClass("active");
            return newActive;
        }
    };

    var findConnectedNodes = function (metroNavigation, node) {
        var connectedNodes = new Array();

        if (settings.only_connected_nodes) {
            connectedNodes.push(node);

            var p = node.parents('li');
            if (p.size() > 0)
                connectedNodes.push(p.get(0));

            node.children('ul').children('li').each(function (index, node) {
                connectedNodes.push(node);
            });
        } else {

            metroNavigation.find('li').each(function (index, node) {
                connectedNodes.push(node);
            });
        }

        return connectedNodes;
    };

    var centerOnNode = function (metroNavigation, node, quick) {

        var navItems = metroNavigation.children('ul');

        var x = metroNavigation.width() / 2 - (node.offset().left - navItems.offset().left);
        var y = metroNavigation.height() / 2 - (node.offset().top - navItems.offset().top);

        if (quick) {
            navItems.css({
                left: x,
                top: y
            });
        } else {
            navItems.animate(
                    {left: x, top: y},
            settings.move_speed,
                    function () {
                    }
            );
        }
    };

    var assemble = function (metroNavigation) {

        var navItems = metroNavigation.find('>ul');

        metroNavigation.removeClass('passive');
        metroNavigation.addClass('active');

        var activeNode = findActiveNode(metroNavigation);

        centerOnNode(metroNavigation, activeNode, true);

        metroNavigation.click(function (event) {

            var x = event.pageX, y = event.pageY;
            var navItems = metroNavigation.children('ul');

            var activeNode = findActiveNode(metroNavigation);

            var closestNode = findNodeByMouse(metroNavigation, activeNode, x, y);

            activeNode.removeClass('active');
            closestNode.addClass('active');

            if (closestNode) {
                centerOnNode(metroNavigation, closestNode, false);
            }
        });

        metroNavigation.find("a").click(function () {

            var activeNode = findActiveNode(metroNavigation);
            var newActiveNode = $(this).parent();

            activeNode.removeClass('active');
            newActiveNode.addClass('active');

            centerOnNode(metroNavigation, newActiveNode, false);

        });

        var reflow = function (quick) {
            var activeNode = findActiveNode(metroNavigation);
            centerOnNode(metroNavigation, activeNode, quick);
        }
        $(window).resize(function () {
            reflow(true);
        });
        $(".split").on("dragstop", function () {
            reflow(false);

            metroNavigation.find(".mover").css({
                bottom: 0,
                right: 0
            });
        });
    };

    metroNavigation.each(function () {
        assemble($(this));
    });
};;$.fn.metroNavigationGrab = function (settingsIn) {

    var metroNavigation = this;

    var bindDragging = function (metroNavigation, navItems) {

        var navItems = metroNavigation.find('>ul');
        var mover = metroNavigation.find(".mover");
        
        var fixedPos = navItems.offset();

        var addPos = function (pos1, pos2) {
            return {
                top: pos1.top + pos2.top,
                left: pos1.left + pos2.left
            };
        };

        mover.draggable({
            start: function () {
                fixedPos = navItems.offset();
            },
            drag: function (e, ui) {
                navItems.css(addPos(fixedPos, ui.position));
            },
            stop: function (e, ui) {
                navItems.css(addPos(fixedPos, ui.position));
                mover.css({
                    top: 0,
                    left: 0
                });
            }
        });

    };

    metroNavigation.each(function () {
        bindDragging($(this));
    });
};;/*
$.fn.metroNavigationPages = function (settingsIn) {

    var metroNavigation = this;
    
    var bindPages = function(metroNavigation) {
        
        metroNavigation.find("a").click(function() {
            
            var content = $("#content");
            
            content.empty();
            content.append("LOADING");
            
            content.load( $(this).attr('href') + " #content" );
            
            return false;
        });
        
    };

    metroNavigation.each(function () {
        bindPages($(this));
    });
};
*/;$(function () {
    //slow scrolling to anchors
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
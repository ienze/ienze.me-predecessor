module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                includePaths: ['./node_modules/foundation-sites/scss/']
            },
            dist: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    'public/stylesheets/app.css': 'private/stylesheets/app.scss'
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    'public/javascripts/linda.min.js': ['private/javascripts/linda/*.js'],
                    'public/javascripts/timeago.min.js': ['private/javascripts/jquery.timeago.js'],
                    'public/javascripts/sticky.min.js': ['private/javascripts/jquery.sticky.js'],
                    'public/javascripts/cookie.min.js': ['node_modules/jquery.cookie/jquery.cookie.js']
                }
            }
        },
        concat: {
          options: {
            separator: ';'
          },
          dist: {
            src: [
              'private/javascripts/linda/*.js',
            ],
            dest: 'public/javascripts/linda.js'
          }
        },
        copy: {
            jquery: {
                src: 'node_modules/jquery/dist/jquery.min.js',
                dest: 'public/javascripts/jquery.min.js'
            },
            foundation: {
                src: 'node_modules/foundation-sites/js/foundation.min.js',
                dest: 'public/javascripts/foundation.min.js'
            },
            foundationVendor: {
                expand: true,
                cwd: 'node_modules/foundation-sites/js/vendor/',
                src: '**/*.js',
                dest: 'public/javascripts/'
            },
            jscroll: {
                src: 'node_modules/jscroll/jquery.jscroll.min.js',
                dest: 'public/javascripts/jscroll.min.js'
            },
            jqueryUiDraggable: {
                src: 'private/javascripts/jquery-ui-draggable.min.js',
                dest: 'public/javascripts/jquery-ui-draggable.min.js'
            }
        },
        criticalcss: {
          custom: {
            options: {
              url: "http://localhost",
              width: 1200,
              height: 900,
              outputfile: "private/critical.css",
              filename: "public/stylesheets/app.css", // Using path.resolve( path.join( ... ) ) is a good idea here
              buffer: 800*1024,
              ignoreConsole: false
            }
          }
        },
        watch: {
            grunt: {files: ['Gruntfile.js']},
            sass: {
                files: ['private/stylesheets/**/*.scss', 'private/javascripts/linda/**/*.js'],
                tasks: ['build']
            }
        },
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-criticalcss');

    grunt.registerTask('build', ['copy', 'sass', 'concat', 'uglify']);
    grunt.registerTask('default', ['watch']);

}
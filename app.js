var fs = require("fs");
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var i18n = require('i18n-2');

var app = express();

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('common', {stream: accessLogStream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//localization
i18n.expressBind(app, {
	locales: ['sk', 'en'],
	defaultLocale: 'sk'
});

//because nginx is used.. DISABLE if it is not!
app.set('trust proxy', true);

//redirect www. to non-www.
function wwwRedirect(req, res, next) {
	if (req.headers.host.slice(0, 4) === 'www.') {
		var newHost = req.headers.host.slice(4);
		return res.redirect(301, req.protocol + '://' + newHost + req.originalUrl);
	}
	next();
}
app.use(wwwRedirect);

//load routes
var routesPath = require("path").join(__dirname, "routes");

fs.readdirSync(routesPath).forEach(function (file) {
	var routeName = '/' + file.substr(0, file.lastIndexOf('.'));
	var route = require("./routes/" + file);

	if (routeName === '/index')
		routeName = '/';

	if (routeName === '/php') {
		routeName = /.+\.php$/;
	}

	if (route) {
		app.use(routeName, route);
	}
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	console.log('DEVELOPMENT MODE');

	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			siteTitle: 'Error',
			title: err.message,
			message: err.message,
			error: err
		});
	});

} else {
	// production error handler
	// no stacktraces leaked to user
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			siteTitle: 'Error',
			title: err.message,
			message: err.message,
			error: {}
		});
	});
}

module.exports = app;

var contentful = require('contentful');

var client = null;
if (process.env.NODE_ENV === 'development') {
	client = contentful.createClient({
		host: 'preview.contentful.com',
		accessToken: 'ff1288679a41d41dcc4df23f7a44209b747db77c0ab1f576f0793dd8edba2cc1',
		space: 'klp36793rhui'
	});
} else {
	client = contentful.createClient({
		accessToken: 'be63e50a3032377b893199761ff00be16e3bc3a654714e276c916e2c61bc7acd',
		space: 'klp36793rhui'
	});
}

var contetTypeIds = {
	'Blog': '30qgOrvLTqUccAw2WQuAus',
	'Project': '7IRpJEUmWImycI4ysqeuMa'
};

var dataLoader = require('./dataLoader.js');
var dataLoaders = {
	blog: dataLoader.init(contetTypeIds.Blog, client),
	project: dataLoader.init(contetTypeIds.Project, client)
};

var printError = function (data, next) {
	console.log(data);
	if (typeof data.details === 'object')
		console.log(data.details.errors);

	if (typeof data === 'object' && data.length === 0) {
		var err = new Error('Not found');
		err.status = 404;
		next(err);
	} else {
		var err = new Error('Internal Server Error');
		err.status = 500;
		next(err);
	}
};

module.exports = {
	client: client,
	entries: function (settings, ok, nok) {
		client.entries(settings).then(ok, nok);
	},
	contetTypeIds: contetTypeIds,
	dataLoaders: dataLoaders,
	printError: printError
};
var i18n = require('i18n-2');

/*
 var createExcerpt = function (text) {
 var maxLenght = 420;
 if (text.length > maxLenght) {
 return text.replace(new RegExp("^(.{" + maxLenght + "}[^\s]*).*"), "$1") + "…";
 }
 return text;
 };
 */

var dataLang = function (lang) {
    if (lang === 'en') {
        return 'en-US';
    } else {
        return lang;
    }
};

var calculateAge = function (birthMonth, birthDay, birthYear) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1)
    {
        age--;
    }

    if (birthMonth - 1 === todayMonth && todayDay < birthDay)
    {
        age--;
    }
    return age;
};

module.exports = {
    settings: {
        siteTitle: 'Ienze',
        siteEmail: 'd.gmiterko@gmail.com',
        siteAuthorYears: calculateAge(6, 23, 1996),
        siteMessageBoardEnabled: false,
		siteDescription: 'SITE_DESCRIPTION',
		siteKeywords: 'SITE_KEYWORDS',
		requestUrl: "http://ienze.me", //default url if not specified
        //templating extension methods
        //createExcerpt: createExcerpt
    },
    extend: function (target) {
        var sources = [].slice.call(arguments, 1);
        sources.forEach(function (source) {
            for (var prop in source) {
                target[prop] = source[prop];
            }
        });
        return target;
    },
    lang: function (req) {
        return {
            current: req.i18n.getLocale(),
            saved: true, //TODO LAME? req.cookies.lang ? true : false,
            dataLang: dataLang(req.i18n.getLocale())
        };
    }
};
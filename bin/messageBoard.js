module.exports = function (server) {
    var io = require('socket.io')(server);

    var authNames = {
        Ienze: 'karmatikak',
        Linda: 'unicorn'
    };
    var blockedNames = ['', 'admin', 'ienze', 'linda', 'lenze', 'Lenze'];

    io.on('connection', function (socket) {

        socket.on('userlogin', function (name) {
            socket._username = name;
            if (isFreeName(name)) {
                socket._authed = true;
                socket.emit('userok');
                io.emit('message', name + " joined chat", {for : 'everyone'});
            } else {
                var token = randomToken();
                socket._authKey = token;
                socket.emit('userauth', token);
            }
        });

        socket.on('userauth', function (pass) {
            if (isCorrectPass(socket._username, socket._authKey, pass)) {
                socket._authed = true;
                socket.emit('userok');
                io.emit('message', socket._username + " joined chat", {for : 'everyone'});
            } else {
                socket.emit('usernok');
            }
        });

        socket.on('message', function (message) {
            if(socket._authed && message && message.trim()) {
                message = socket._username + ": " + message;
                io.emit('message', message, {for : 'everyone'});
                console.log("MESSAGE_BOARD " + message);
            }
        });

        socket.on('disconnect', function () {
            if (socket._username && socket._authed)
                io.emit('message', socket._username + " leaved chat", {for : 'everyone'});
        });

    });

    randomToken = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 42; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };

    var isFreeName = function (name) {
        if (blockedNames.indexOf(name) >= 0)
            return false;
        if (Object.keys(authNames).indexOf(name) >= 0)
            return false;
        return true;
    };

    var isCorrectPass = function (name, authKey, pass) {
        return (authNames[name] + authKey === pass);
    };
};
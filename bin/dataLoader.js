
var init = function (contentType, content) {

	var parseFields = function (fields) {

		for (var index in fields) {

			//pasrse assets
			if (typeof fields[index] === 'object') {

				if (fields[index]) {
					//single asset
					if (fields[index].sys
							&& fields[index].sys.type === 'Asset')
						fields[index] = fields[index].fields.file;

					//multiple assets
					if (fields[index][0] && fields[index][0].sys
							&& fields[index][0].sys.type === 'Asset') {
						var assets = fields[index];
						fields[index] = [];
						for (var indexAsset in assets) {
							fields[index][indexAsset] = assets[indexAsset].fields.file;
						}
					}
				}
			}
		}

		return fields;
	};

	var parseData = function (data, single) {

		if (single) {
			var fields = parseFields(data[0].fields);
			fields.id = data[0].sys.id;
			return fields;
		} else {
			var items = [];

			for (var index in data) {
				var fields = parseFields(data[index].fields);
				fields.id = data[index].sys.id;
				items.push(fields);
			}

			return items;
		}

	};

	var fetchOne = function (id, ok, nok, customSettings) {

		var settings = {
			'content_type': contentType,
			'fields.slug': id
		};

		if (customSettings)
			settings = require('../bin/siteSettings.js').extend({}, settings, customSettings);

		content.entries(settings).then(function (data) {
			if (data && data.length > 0) {
				ok(parseData(data, true));
			} else {
				nok(data);
			}
		}, function (data) {
			nok(data);
		});

	};

	var fetchMore = function (skip, amount, ok, nok, customSettings) {

		if (!skip)
			skip = 0;

		if (!amount)
			amount = 100;

		var settings = {
			'content_type': contentType,
			'skip': skip,
			'limit': amount
		};

		if (customSettings)
			settings = require('../bin/siteSettings.js').extend({}, settings, customSettings);

		content.entries(settings).then(function (data) {
			ok(parseData(data, false));

		}, function (data) {
			nok(data);
		});

	};

	return {
		fetchOne: fetchOne,
		fetchMore: fetchMore
	};

};

module.exports = {
	init: init
};